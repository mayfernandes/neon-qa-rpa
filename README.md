# Gerar clientes Fake
Esse RPA gerará clientes aleatórios PF e/ou PJ via INSERT na ClientRegister, fará os UPDATEs necessários (simulação de crédito) e publicará a mensagem na fila *onboard.queue.clientconfirm.principal* do RabbitMQ de HML. Ao final ele logará o nome, e-mail, documento e registerId criados, basta utilizar!

Após mensagens postadas na fila citada acima, basta executar a **Neon.ProcessApproval.Console** mais recente disponível.

## Pré-requisitos
- Python 3
    - MAC: `brew install python3`

## Instalação
`python -m pip install -r requirements.txt`
ou
`python3 -m pip3 install -r requirements.txt`

## Plugin VSCode

Caso queira ver ou alterar o código, utilize VS Code com o plugin:
- [Robocorp - Robot Framework Language Server](https://marketplace.visualstudio.com/items?itemName=robocorp.robotframework-lsp)

## Execução

Execute informando o **TIPO** (PF ou PJ) e **QTDE** desejada de cada tipo.

Obs.: Se ocorrer erro na execução do comando *robot*, tente colocar o `python -m` na frente do comando, exemplo: `python -m robot insert_client_register.robot`

- Exemplo de comando para criar **10 pessoas PF**:

    `robot -v TIPO:PF -v NUMBER_OF_CLIENTS:10 insert_client_register.robot`

- Exemplo de comando para criar **5 pessoas PJ**:

    `robot -v TIPO:PJ -v NUMBER_OF_CLIENTS:5 insert_client_register.robot`