*** Settings ***
Library         RabbitMqHttp
Library         FakerLibrary  locale=pt_BR
Library         OperatingSystem
Library         String
Library         Collections
Resource        ./common/database/database_commons.resource
Suite Setup     Conectar no RabbitMq
Suite Teardown  Close All Rabbitmq Connections

*** Variables ***
${FILA}               onboard.queue.clientconfirm.principal
${TIPO}               PF
${NUMBER_OF_CLIENTS}  3


*** Tasks ***
Criar clientes fake
    FOR  ${i}  IN RANGE  0  ${NUMBER_OF_CLIENTS}
      Gerar dados FAKE de um cliente
      Inserir os dados no Banco de Dados - ClientRegister  ${TIPO}
      Inserir Simulação de Crédito
      ${register_id}  Obter id cadastrado
      ${payload}  Criar payload da fila  ${register_id}
      Postar na fila  ${payload}
    END


*** Keywords ***
Conectar no RabbitMq
    Create Rabbitmq Connection    host=http://rabbithml.qadomain.local
    ...                           http_port=15672
    ...                           username=neonAppHml
    ...                           password=rabbitneonhml
    ...                           alias=rabbit_hml

Gerar dados FAKE de um cliente
    ${NOME}     FakerLibrary.name_male
    ${CPF}      FakerLibrary.cpf
    ${CPF}      Remove String Using Regexp   ${CPF}   [^0-9a-zA-Z:,]+
    ${EMAIL}    FakerLibrary.email
    ${CNPJ}     FakerLibrary.cnpj
    ${CNPJ}     Remove String Using Regexp   ${CNPJ}   [^0-9a-zA-Z:,]+
    ${CEP}      FakerLibrary.postcode
    ${CIDADE}   FakerLibrary.city
    ${ESTADO}   FakerLibrary.state

    ${client_register}  Create Dictionary  nome=${NOME}  cpf=${CPF}  email=${EMAIL}
    ...                 cnpj=${CNPJ}  postalcode=${CEP}  city=${CIDADE}  state=${ESTADO}

    Set Suite Variable  ${client_register}

Inserir os dados no Banco de Dados - ClientRegister
    [Arguments]     ${TIPO}
    IF  '${TIPO}' == 'PF'
        Set To Dictionary	${client_register}	identifier=${client_register.cpf}
    ELSE
        Set To Dictionary	${client_register}	identifier=${client_register.cnpj}        
    END
    Log To Console  ${\n}------- USUARIO --------
    Log To Console  ${\n}Nome: ${client_register.nome}${\n}DOCUMENTO: ${client_register.identifier}${\n}E-mail: ${client_register.email}
    ${sql_string}   Get File             ${CURDIR}/sql/insere_client_register.sql
    ${sql_string}   Replace Variables    ${sql_string}
    Executar String SQL  NEON_POTTENCIAL_HML  ${sql_string}

Inserir Simulação de Crédito    
    ${sql_string}   Get File             ${CURDIR}/sql/insere_simulacao_credito.sql
    ${sql_string}   Replace Variables    ${sql_string}
    Executar String SQL  CREDIT_BV_INTEGRATION  ${sql_string}

Obter id cadastrado
    ${output}       Executar Select Query    NEON_POTTENCIAL_HML
    ...             select id from clientregister where cpf = '${client_register.identifier}'
    Log To Console  ${\n}Register ID criado: ${output[0][0]}
    [Return]        ${output[0][0]}

Criar payload da fila
    [Arguments]     ${register_id}
    ${payload}      Replace Variables  
    ...             {"ClientRegisterId":${register_id},"PanelUserId":241,"Reason":"Teste","ApprovalUserId":241}
    [Return]        ${payload}

Postar na fila
    [Arguments]  ${payload}
    Log To Console    ${\n}Publicando na fila: ${payload}
    Publish Message   amq.default    ${FILA}    ${payload}